    // swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

//2.0.4-beta.5

let package = Package(
    name: "dotveep xcfamework",
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "dotveep-static",
            targets: ["dotveep-static"]),
        .library(
            name: "dotveep-dynamic",
            targets: ["dotveep-dynamic"]),
        
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
//        .binaryTarget(name:"dotveep", path:"../dotveep2/xcframeworks/dotveep-static/dotveep.xcframework" ),
        .binaryTarget(
            name: "dotveep-static",
            url: "https://github.com/veepionyc/dotveep-distribution/raw/2.0.1/xcframeworks/static/dotveep.xcframework.zipp",
            checksum: "210da9aa6378c805807da4d42c7c7aba46761b755b7aa44fc2155340a25bb2b2"),
        .binaryTarget(
            name: "dotveep-dynamic",
            url: "https://github.com/veepionyc/dotveep-distribution/raw/2.0.1/xcframeworks/dynamic/dotveep.xcframework.zip",
            checksum: "865b74e0f931e9027203e4a29bdbbb9d486efb0ab3425b9fd63fc39d32fee637")
        
        
       
        
        
    ]
)
